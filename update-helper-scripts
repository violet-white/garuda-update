#!/bin/bash
set -e

update_keyrings() {
    local packages=("archlinux-keyring" "chaotic-keyring" "garuda-hotfixes")
    if $PACMAN -Qq blackarch-keyring &> /dev/null; then
        packages+=("blackarch-keyring")
    fi
    if [ ! -z "$($PACMAN -Qu ${packages[@]} 2>&1)" ]; then
        echo -e "\n\033[1;33m-->\033[1;34m Applying keyring updates before starting full system update..\033[0m"
        SNAP_PAC_SKIP=y SKIP_AUTOSNAP= $PACMAN -S --needed --noconfirm ${packages[@]} || false
    fi
}

fix_jack2_confilict() {
    if [ -e /usr/bin/pw-jack ]; then
        local jack2=false
        local jack2_dbus=false
        local lib32_jack=false
        $PACMAN -Qq jack2 &> /dev/null && jack2=true
        $PACMAN -Qq jack2-dbus &> /dev/null && jack2=true
        $PACMAN -Qq lib32-jack2 &> /dev/null && lib32_jack=true
        if [ "$jack2" == true ] || [ "$jack2_dbus" == true ]; then
            echo -e "\n\033[1;33m-->\033[1;34m Addressing jack2 conflict..\033[0m"
        fi
        if [ "$jack2_dbus" == true ]; then
            $PACMAN -R jack2-dbus --noconfirm
        fi
        if [ "$jack2" == true ]; then
            $PACMAN -Rdd jack2 --noconfirm
        fi
        if [ "$lib32_jack" == true ]; then
            # Most hacky thing I've ever done
            # Goal: Get pacman to properly install lib32-pipewire-jack next system update
            # UPDATE 2022-04-15: I mean, now that we have auto-pacman, I *could* fix this in a more sane way, but then again I'm lazy and I don't have an install out of date enough to test it anyway.
            # This won't execute on any recent system ever, don't worry. Contributions welcome.
            SNAP_PAC_SKIP=y SKIP_AUTOSNAP= $PACMAN --dbonly -U https://archive.archlinux.org/repos/2021/12/31/multilib/os/x86_64/lib32-pipewire-jack-1%3A0.3.42-2-x86_64.pkg.tar.zst --noconfirm
            $PACMAN -Rdd lib32-jack2 --noconfirm
        fi
    fi
}

fix_wireplumber_conflict() {
    if [ -e /usr/bin/wireplumber ] && [ -e /usr/bin/pipewire-media-session ]; then
        echo -e "\n\033[1;33m-->\033[1;34m Addressing pipewire-media-session/wireplumber conflict..\033[0m"
        $PACMAN -Rdd wireplumber --noconfirm
    fi
}

# If we are dealing with a more legacy garuda install, we should auto install expect to get auto-pacman working.
# Let's use a confirmed working version for legacy tclsh versions though instead of praying the repo version will work.
install_expect() {
    if [ -x /usr/bin/tclsh8.6 ] && [ ! -x /usr/bin/expect ]; then
        SKIP_AUTOSNAP=1 SNAP_PAC_SKIP=y pacman -U https://archive.archlinux.org/repos/2022/04/25/extra/os/x86_64/expect-5.45.4-2-x86_64.pkg.tar.zst --noconfirm --asdeps
    fi
}

# Anything printed by this script into stdout will be added as a pacman parameter to pacman -Syu
package-replaces() {
    packages="$($PACMAN -Qq python-xdg garuda-dr460nized garuda-xfce-kwin-settings garuda-lxqt-kwin-settings garuda-wayfire-settings sweet-kde-git sweet-cursor-theme-git 2> /dev/null | xargs || true)"
    # We replace python-xdg with python-pyxdg from community
    # This is not done automatically for some reason
    if [[ "$packages" =~ (^| )python-xdg($| ) ]] &> /dev/null; then
        echo python-pyxdg
    fi
    if [[ "$packages" =~ (^| )(garuda-dr460nized|garuda-xfce-kwin-settings|garuda-lxqt-kwin-settings|garuda-wayfire-settings)($| ) ]] && [[ "$packages" =~ (^| )(sweet-cursor-theme-git|sweet-kde-git)($| ) ]]; then
        echo sweet-theme-full-git
        echo --ignore
        echo sweet-kde-git,sweet-cursor-theme-git,kvantum-theme-sweet-git,sweet-gtk-theme-dark,plasma5-theme-sweet-git
    fi
}

pre-update-routines() {
    update_keyrings
    fix_wireplumber_conflict
    fix_jack2_confilict
    install_expect
}

"$@"
